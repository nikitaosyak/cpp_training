#include <stdio.h>

#include <SDL.h>
#define GL3_PROTOTYPES 1
#include <GL/glew.h>

const char* SCREEN_TITLE = "opengl3.2 simple setup";
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 1024;

bool init();
void checkDriverInfo();
void setGlAttributes();
void close();

SDL_Window* window = NULL;
SDL_GLContext gl = NULL;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {

        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_OPENGL;
        window = SDL_CreateWindow(SCREEN_TITLE, xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {

            checkDriverInfo();
            setGlAttributes();

            gl = SDL_GL_CreateContext(window);
            if (gl == NULL) {
                printf("gl context could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                printf("gl context created successfully\n");

                SDL_GL_SetSwapInterval(1); // vsync

                glewExperimental = GL_TRUE;
                glewInit();
            }
        }
    }

    return success;
}

void checkDriverInfo()
{
    printf("----------\n");
    int drAvailable = SDL_GetNumRenderDrivers();
    if (drAvailable < 1) {
        printf("no drivers found: %s\n", SDL_GetError());
    } else {
        printf("drivers found: %i\n", drAvailable);
        for (int i = 0; i < drAvailable; i++) {
            SDL_RendererInfo renderInfo;
            SDL_GetRenderDriverInfo(i, &renderInfo);
            printf("driver %i: %s\n", i, renderInfo.name);
        }
    }
    printf("----------\n");
}

void setGlAttributes()
{
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
}

void close()
{
    SDL_GL_DeleteContext(gl);
    gl = NULL;

    SDL_DestroyWindow(window);
    window = NULL;

    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    printf("initialization success\n");

    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    SDL_GL_SwapWindow(window);

    bool quit = false;
    SDL_Event event;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            } else if (event.type == SDL_KEYDOWN) {
                bool redraw = false;
                if (event.key.keysym.sym == SDLK_r) {
                    glClearColor(1.0, 0.0, 0.0, 1.0);
                    redraw = true;
                } else if (event.key.keysym.sym == SDLK_g) {
                    glClearColor(0.0, 1.0, 0.0, 1.0);
                    redraw = true;
                } else if (event.key.keysym.sym == SDLK_b) {
                    glClearColor(0.0, 0.0, 1.0, 1.0);
                    redraw = true;
                }

                if (redraw) {
                    glClear(GL_COLOR_BUFFER_BIT);
                    SDL_GL_SwapWindow(window);
                }
            }

            const Uint8* keyStates = SDL_GetKeyboardState(NULL);
            if (keyStates[SDL_SCANCODE_ESCAPE]) {
                quit = true;
            }
        }
    }

    close();
    return 0;
}