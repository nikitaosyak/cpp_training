#pragma once

#include <GL/glew.h>
#include <SDL.h>

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

class Shader
{
public:
    bool init()
    {
        _shaderProgram = glCreateProgram();
        bindAttribute(0, "in_position");
        bindAttribute(1, "in_color");

        if (!loadShader("shader/vert.glsl", &_vShader, GL_VERTEX_SHADER)) 
            return false;
        if (!loadShader("shader/geom.glsl", &_gShader, GL_GEOMETRY_SHADER))
            return false;
        if (!loadShader("shader/frag.glsl", &_fShader, GL_FRAGMENT_SHADER)) 
            return false;

        return linkShaders();
    }

    void useProgram() { 
        glUseProgram(_shaderProgram); 
    }

    void cleanup()
    {
        glUseProgram(0);

        glDetachShader(_shaderProgram, _vShader);
        glDetachShader(_shaderProgram, _gShader);
        glDetachShader(_shaderProgram, _fShader);
        
        glDeleteProgram(_shaderProgram);
        glDeleteShader(_vShader);
        glDeleteShader(_gShader);
        glDeleteShader(_fShader);
    }

private:
    GLuint _shaderProgram;
    GLuint _vShader, _gShader, _fShader;

    void bindAttribute(int idx, const std::string &attr)
    {
        glBindAttribLocation(_shaderProgram, idx, attr.c_str());
    }

    bool loadShader(const std::string &path, GLuint* shaderId, GLenum shaderType)
    {

        std::cout << "loading contents of " << path.c_str() << std::endl;
        
        std::ifstream t(path.c_str());
        std::stringstream buffer;
        buffer << t.rdbuf();
        std::string fileContent = buffer.str();
        char* src = const_cast<char*>(fileContent.c_str());
        int32_t len = fileContent.length();

        std::cout << "load&compile shader in gpu" << std::endl;

        *shaderId = glCreateShader(shaderType);
        glShaderSource(*shaderId, 1, &src, &len);
        glCompileShader(*shaderId);

        int wasCompiled = 0;
        glGetShaderiv(*shaderId, GL_COMPILE_STATUS, &wasCompiled);
        if (wasCompiled == 0) {
            std::cout << "---------------------\n" << std::endl;
            std::cout << "shader compilation failed: " << std::endl;

            int maxLen;
            glGetShaderiv(*shaderId, GL_INFO_LOG_LENGTH, &maxLen);

            char* shaderInfoLog = new char[maxLen];
            glGetShaderInfoLog(*shaderId, maxLen, &maxLen, shaderInfoLog);
            std::cout << "\terror info: " << shaderInfoLog << std::endl;

            std::cout << "---------------------\n" << std::endl;
            delete shaderInfoLog;
        }

        glAttachShader(_shaderProgram, *shaderId);
        return true;   
    }

    bool linkShaders()
    {
        std::cout << "linking shaders.." << std::endl;
        glLinkProgram(_shaderProgram);

        int isLinked;
        glGetProgramiv(_shaderProgram, GL_LINK_STATUS, &isLinked);
        if (isLinked == false) {
            std::cout << "---------------------\n" << std::endl;
            std::cout << "shader linking failed : " << std::endl;

            int maxLen;
            glGetProgramiv((int32_t)_shaderProgram, GL_INFO_LOG_LENGTH, &maxLen);
            std::cout << "info len : " << maxLen << std::endl;

            char* shaderProgramInfoLog = new char[maxLen];
            glGetProgramInfoLog(_shaderProgram, maxLen, &maxLen, shaderProgramInfoLog);
            std::cout << "linker error msg : " << shaderProgramInfoLog << std::endl;

            std::cout << "---------------------\n" << std::endl;
            delete shaderProgramInfoLog;
            return false;
        }

        return isLinked != 0;
    }
};