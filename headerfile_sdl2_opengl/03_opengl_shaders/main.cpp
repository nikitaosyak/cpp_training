#include <stdio.h>

#include <SDL.h>
#define GL3_PROTOTYPES 1
#include <GL/glew.h>

#include "Shader.h"

const char* SCREEN_TITLE = "opengl3.2 basic shaders";
const int SCREEN_WIDTH = 512;
const int SCREEN_HEIGHT = 512;

bool init();
void checkDriverInfo();
void setGlAttributes();
bool setupBuffers();
void render();
void close();

SDL_Window* window = NULL;
SDL_GLContext gl = NULL;

const int points = 12;
const int floatsPerVertice = 3;
const int floatsPerColor = 4;
// quad vertices data in left-hand oriented space
const GLfloat quadVerticies[points][floatsPerVertice] = {
    {  0.2,  0.2,  0.5 }, // Top right
    { -0.2,  0.2,  0.5 }, // Top left
    {  0.0,  0.0,  0.5 }, // Center

    {  0.2,  0.2,  0.5 }, // Top right
    {  0.2, -0.2,  0.5 }, // Bottom right 
    {  0.0,  0.0,  0.5 }, // Center

    { -0.2, -0.2,  0.5 }, // Bottom left
    {  0.2, -0.2,  0.5 }, // Bottom right 
    {  0.0,  0.0,  0.5 }, // Center

    { -0.2, -0.2,  0.5 }, // Bottom left
    { -0.2,  0.2,  0.5 }, // Top left
    {  0.0,  0.0,  0.5 }, // Center
};
// quad color data
const GLfloat quadColors[points][floatsPerColor] = {
    { 0.5, 0.5, 0.5, 1.0f }, // Top right
    { 0.5, 0.5, 0.5, 1.0f }, // Bottom right 
    { 0.0, 0.0, 0.0, 1.0f }, // Center

    { 0.5, 0.5, 0.5, 1.0f }, // Top left
    { 0.5, 0.5, 0.5, 1.0f }, // Top right
    { 0.0, 0.0, 0.0, 1.0f }, // Center

    { 0.5, 0.5, 0.5, 1.0f }, // Bottom left
    { 0.5, 0.5, 0.5, 1.0f }, // Bottom right 
    { 0.0, 0.0, 0.0, 1.0f }, // Center

    { 0.5, 0.5, 0.5, 1.0f }, // Bottom left
    { 0.5, 0.5, 0.5, 1.0f }, // Top left
    { 0.0, 0.0, 0.0, 1.0f }, // Center
};

// allocating storage for vbo's and vao's pointers in GPU
const uint32_t vboCount = 2;
const uint32_t vaoCount = 1;
GLuint vbos[vboCount];
GLuint vaos[vaoCount];

const uint32_t verticiesAttrIndex = 0;
const uint32_t colorAttrIndex = 1;

Shader shader;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {

        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_OPENGL;
        window = SDL_CreateWindow(SCREEN_TITLE, xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {

            checkDriverInfo();
            setGlAttributes();

            gl = SDL_GL_CreateContext(window);
            if (gl == NULL) {
                printf("gl context could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                printf("gl context created successfully\n");

                SDL_GL_SetSwapInterval(1); // vsync

                glewExperimental = GL_TRUE;
                glewInit();
            }
        }
    }

    return success;
}

void checkDriverInfo()
{
    printf("----------\n");
    int drAvailable = SDL_GetNumRenderDrivers();
    if (drAvailable < 1) {
        printf("no drivers found: %s\n", SDL_GetError());
    } else {
        printf("drivers found: %i\n", drAvailable);
        for (int i = 0; i < drAvailable; i++) {
            SDL_RendererInfo renderInfo;
            SDL_GetRenderDriverInfo(i, &renderInfo);
            printf("driver %i: %s\n", i, renderInfo.name);
        }
    }
    printf("----------\n");
}

void setGlAttributes()
{
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
}

bool setupBuffers()
{
    printf("settings up buffers..\n");
    
    // generate vbos and vao in GPU
    glGenBuffers(vboCount, vbos);
    glGenVertexArrays(vaoCount, vaos);
    glBindVertexArray(vaos[0]); // point to current vao


    // -- verticies to vao
    // 
    glBindBuffer(GL_ARRAY_BUFFER, vbos[0]); // point to current vbo
    
    // fill vbo with vertice data
    uint32_t vboSize = (points * floatsPerVertice) * sizeof(GLfloat);
    printf("size of quad verticies vbo: %i\n", vboSize);
    glBufferData(GL_ARRAY_BUFFER, vboSize, quadVerticies, GL_STATIC_DRAW);
    // assign vertice data to index of 0 of vao
    glVertexAttribPointer(verticiesAttrIndex, floatsPerVertice, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(verticiesAttrIndex);

    // -- colors to vao
    // 
    glBindBuffer(GL_ARRAY_BUFFER, vbos[1]);

    // fill vbo with color data
    vboSize = (points * floatsPerColor) * sizeof(GLfloat);
    printf("size of quad colors vbo: %i\n", vboSize);
    glBufferData(GL_ARRAY_BUFFER, vboSize, quadColors, GL_STATIC_DRAW);
    // assign color data to index 1 of vao
    glVertexAttribPointer(colorAttrIndex, floatsPerColor, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(colorAttrIndex);

    if (!shader.init()) return false;
    shader.useProgram();

    glBindBuffer(GL_ARRAY_BUFFER, 0); // restore binding to default

    return true;
}

void render()
{
    glClearColor(0.1, 0.1, 0.1, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glDrawArrays(GL_TRIANGLES, 0, points); // ??
    SDL_GL_SwapWindow(window); // swap front and back buffer
}

void close()
{
    shader.cleanup();

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDeleteBuffers(vboCount, vbos);
    glDeleteVertexArrays(vaoCount, vaos);

    SDL_GL_DeleteContext(gl);

    SDL_DestroyWindow(window);

    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    printf("initialization success\n");

    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    SDL_GL_SwapWindow(window);

    if (!setupBuffers()) return 1;

    bool quit = false;
    SDL_Event event;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            }

            const Uint8* keyStates = SDL_GetKeyboardState(NULL);
            if (keyStates[SDL_SCANCODE_ESCAPE]) {
                printf("got SDL_QUIT\n");
                quit = true;
            }
        }

        render();
    }

    close();
    return 0;
}