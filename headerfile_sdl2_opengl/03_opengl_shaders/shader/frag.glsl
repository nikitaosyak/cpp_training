#version 330

precision highp float;

in vec4 ex_color;

out vec4 color;

float rand(vec2 input)
{
    highp float a = 13;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt= dot(input.xy ,vec2(a,b));
    highp float sn= mod(dt,3.14);
    return fract(sin(sn) * c);
}

void main(void)
{
    color.r = (ex_color.r * 0.1) + (rand(ex_color.ra) * 0.5);
    color.g = (ex_color.g * 0.1) + (rand(ex_color.bg) * 0.5);
    color.b = (ex_color.b * 0.3) + (rand(ex_color.gb) * 0.5);
    color.a = ex_color.a;
    // color = ex_color;
}