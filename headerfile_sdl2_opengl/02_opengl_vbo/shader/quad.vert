#version 150

attribute vec3 in_position;
attribute vec4 in_color;

out vec4 ex_color;

void main(void)
{
    gl_Position = vec4(in_position.x, in_position.y, 0.0, 1.0);

    ex_color = in_color;
}