#pragma once

// #include <GL/glew.h>
#include <SDL.h>

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

class Shader
{
public:
    bool init()
    {
        _shaderProgram = glCreateProgram();
        bindAttrLocation(0, "in_position");
        bindAttrLocation(1, "in_color");

        if (!loadVertexShader("shader/quad.vert")) return false;

        if (!loadFragmentShader("shader/quad.frag")) return false;

        return linkShaders();
    }

    void useProgram() { glUseProgram(_shaderProgram); }

    void cleanup()
    {
        glUseProgram(0);

        glDetachShader(_shaderProgram, _vShader);
        glDetachShader(_shaderProgram, _fShader);
        
        glDeleteProgram(_shaderProgram);
        glDeleteShader(_vShader);
        glDeleteShader(_fShader);
    }

private:
    GLuint _shaderProgram;
    GLuint _vShader, _fShader;

    void bindAttrLocation(int idx, const std::string &attr)
    {
        glBindAttribLocation(_shaderProgram, idx, attr.c_str());
    }

    bool loadVertexShader(const std::string &path)
    {
        std::cout << "loading vertex shader.." << std::endl;

        std::string strContents = readFile(path.c_str());
        char* src = const_cast<char*>(strContents.c_str());
        int32_t len = strContents.length();

        _vShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(_vShader, 1, &src, &len);
        glCompileShader(_vShader);

        int wasCompiled = 0;
        glGetShaderiv(_vShader, GL_COMPILE_STATUS, &wasCompiled);
        if (wasCompiled == 0) {
            printShaderCompilationErrorInfo(_vShader);
            return false;
        }

        glAttachShader(_shaderProgram, _vShader);
        return true;
    }

    bool loadFragmentShader(const std::string &path)
    {
        std::cout << "loading fragment shader.." << std::endl;

        std::string strContents = readFile(path.c_str());
        char* src = const_cast<char*>(strContents.c_str());
        int32_t len = strContents.length();

        _fShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(_fShader, 1, &src, &len);
        glCompileShader(_fShader);

        int wasCompiled = 0;
        glGetShaderiv(_fShader, GL_COMPILE_STATUS, &wasCompiled);
        if (wasCompiled == 0) {
            printShaderCompilationErrorInfo(_fShader);
            return false;
        }

        glAttachShader(_shaderProgram, _fShader);
        return true;
    }

    bool linkShaders()
    {
        std::cout << "linking shaders.." << std::endl;
        glLinkProgram(_shaderProgram);

        int isLinked;
        glGetProgramiv(_shaderProgram, GL_LINK_STATUS, &isLinked);
        if (isLinked == false) {
            std::cout << "---------------------\n" << std::endl;
            std::cout << "shader linking failed : " << std::endl;

            int maxLen;
            glGetProgramiv((int32_t)_shaderProgram, GL_INFO_LOG_LENGTH, &maxLen);
            std::cout << "info len : " << maxLen << std::endl;

            char* shaderProgramInfoLog = new char[maxLen];
            glGetProgramInfoLog(_shaderProgram, maxLen, &maxLen, shaderProgramInfoLog);
            std::cout << "linker error msg : " << shaderProgramInfoLog << std::endl;

            std::cout << "---------------------\n" << std::endl;
            delete shaderProgramInfoLog;
            return false;
        }

        return isLinked != 0;
    }

    std::string readFile(const char* file)
    {
        std::ifstream t(file);
        std::stringstream buffer;
        buffer << t.rdbuf();

        std::string fileContent = buffer.str();
        return fileContent;
    }

    void printShaderCompilationErrorInfo(int32_t shaderId)
    {
        std::cout << "---------------------\n" << std::endl;
        std::cout << "shader compilation failed: " << std::endl;

        int maxLen;
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLen);

        char* shaderInfoLog = new char[maxLen];
        glGetShaderInfoLog(shaderId, maxLen, &maxLen, shaderInfoLog);
        std::cout << "\terror info: " << shaderInfoLog << std::endl;

        std::cout << "---------------------\n" << std::endl;
        delete shaderInfoLog;
    }
};