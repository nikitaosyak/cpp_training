#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <SDL2/SDL_video.h>
#include <string>

#include "Model.h"
#include "Shader.h"

using namespace std;

class Renderer
{
public:
    Renderer() {}

    void init()
    {
        _proj = glm::perspective(
            45.0f,      // FOV
            1.0f/1.0f,  // aspect
            0.1f,       // near plane
            100.0f      // far plane
        );

        _view = glm::lookAt(
            glm::vec3(1,  1, -2),   // camera center
            glm::vec3(0,  0,  0),   // where to look
            glm::vec3(0,  1,  0)    // up vector
        );

        // opengl attributes
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        // opengl options
        // enable transparent blending
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // enable depth test
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        cout << "Renderer: init" << endl;
    }

    bool setupShader(const std::string &vPath, const std::string &fPath)
    {
        bool success = true;

        success = _shader.init();
        if (success) {
            success = _shader.loadShader(vPath, GL_VERTEX_SHADER);
            if (success) {
                success = _shader.loadShader(fPath, GL_FRAGMENT_SHADER);
                if (success) {
                    success = _shader.linkShaders();
                }
            }
        }

        cout << "Renderer: shader setup successfull" << endl;
        return success;
    }

    void setModelShader(Model &model) { model.setShader(_shader); }

    void renderClear()
    {
        glClearColor(0.5, 0.5, 0.5, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void setModelMatrix(const glm::mat4 &modelMatrix)
    {
        _shader.setMatrix(_proj * _view * modelMatrix);
    }

    void renderModel(const Model &model)
    {
        model.render();
    }

    void renderSwap(SDL_Window* window)
    {
        SDL_GL_SwapWindow(window);
    }

    void cleanup()
    {
        cout << "Renderer: cleanup" << endl;
        _shader.cleanup();
    }

private:
    Shader _shader;

    glm::mat4 _proj;
    glm::mat4 _view;
};
