#pragma once

#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <cstdint>
#include <iostream>
#include <vector>

#include "Shader.h"
#include "utils.h"

using namespace std;

class Model
{
public:
    Model() {}

    void setShader(Shader value) { _shader = value; }

    void setMVP(glm::mat4 value)
    {
        _shader.useProgram();
        _shader.setMatrix(value);
    }

    glm::mat4 getMatrix() { return _tMatrix; }
    void resetMatrix() { _tMatrix = glm::mat4(1.0f); }
    void translateMatrix(const glm::vec3 &axis) { _tMatrix = glm::translate(_tMatrix, axis); }
    void rotateMatrix(const float angle) {
        _tMatrix = glm::rotate(_tMatrix, angle, glm::vec3(0.0f, 1.0f, 0.0f));
    }

    bool setupBufferObjects()
    {
        cout << "Model: will setup VBOs" << endl;
        // read model from text files
        _verticies = utils::readModelFile("model/positions");
        _colors = utils::readModelFile("model/colors");

        // generate buffer objects and array in GPU
        glGenBuffers(2, _vbo);
        glGenVertexArrays(1, _vao);
        glBindVertexArray(_vao[0]); // point to current array


        // setup verticies
        // --
        glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]); // point to vertices buffer object

        uint32_t vSize = _verticies.size() * sizeof(GLfloat);
        glBufferData(GL_ARRAY_BUFFER, vSize, &_verticies[0], GL_STATIC_DRAW);
        glVertexAttribPointer(_verticeAttrIndex, 3, GL_FLOAT, GL_FALSE, 0, 0);

        // setup colors
        // --
        glBindBuffer(GL_ARRAY_BUFFER, _vbo[1]); // point to colors buffer object

        uint32_t cSize = _colors.size() * sizeof(GLfloat);
        glBufferData(GL_ARRAY_BUFFER, cSize, &_colors[0], GL_STATIC_DRAW);
        glVertexAttribPointer(_colorAttrIndex, 4, GL_FLOAT, GL_FALSE, 0, 0);

        //
        glEnableVertexAttribArray(_verticeAttrIndex);
        glEnableVertexAttribArray(_colorAttrIndex);

        glBindBuffer(GL_ARRAY_BUFFER, 0); // restore buffer binding
        glBindVertexArray(_vao[0]);

        cout << "Model: buffers setup success" << endl;
        return true;
    }

    void render() const
    {
        _shader.useProgram();
        glDrawArrays(GL_TRIANGLES, 0, _verticies.size() / 3);
    }

    void cleanup()
    {
        cout << "Model: cleanup" << endl;
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDeleteBuffers(2, _vbo);
        glDeleteVertexArrays(1, _vao);
    }


private:
    std::vector<GLfloat> _verticies;
    std::vector<GLfloat> _colors;

    GLuint _vbo[2], _vao[1];

    const uint32_t _verticeAttrIndex = 0;
    const uint32_t _colorAttrIndex = 1;

    glm::mat4 _tMatrix;
    Shader _shader;
};
