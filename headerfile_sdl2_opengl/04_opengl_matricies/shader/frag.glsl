#version 330

precision highp float;

in vec4 color;

out vec4 fragColor;

void main(void)
{
    fragColor = color;
}