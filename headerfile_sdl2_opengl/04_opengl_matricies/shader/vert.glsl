#version 330

in vec3 in_position;
in vec4 in_color;

uniform mat4 mvp;

out vec4 color;

void main(void)
{
    vec4 pos = vec4(in_position, 1.0);
    gl_Position = mvp * pos;

    color = in_color;    
}