#pragma once

#include <GL/glew.h>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

namespace utils
{
    void printShaderLinkingError(GLuint);
    void printShaderCompilationError(int32_t);
    vector<GLfloat> readModelFile(const char*);
    string readShaderFile(const char*);

    void printShaderLinkingError(GLuint programId)
    {
        cout << "----------------" << endl;
        cout << "shader linking failed: " << endl;

        int maxLen;
        glGetProgramiv((int32_t)programId, GL_INFO_LOG_LENGTH, &maxLen);

        cout << "info len: " << maxLen << endl;

        char* log = new char[maxLen];
        glGetProgramInfoLog(programId, maxLen, &maxLen, log);
        cout << "error message: " << log << endl;
//
        delete [] log;
        cout << "----------------" << endl;
    }

    void printShaderCompilationError(int32_t shaderId)
    {
        int maxLen;
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &maxLen);

        char* infoLog = new char[maxLen];
        glGetShaderInfoLog(shaderId, maxLen, &maxLen, infoLog);

//        string log = infoLog;

        cout << "----------------" << endl;
        cout << "shader compile failed: " << endl;
        cout << infoLog << endl;
        cout << "----------------" << endl;

        delete [] infoLog;
    }

    vector<GLfloat> readModelFile(const char* location)
    {
        ifstream t(location);

        vector<GLfloat> result;
        cout << "reading model file: " << location << endl;

        while(t.good()) {
            string s;
            t >> s;

            GLfloat f = atof(s.c_str());
            result.push_back(f);
        }

        return result;
    }

    string readShaderFile(const char* location)
    {
        ifstream t(location);

        stringstream buffer;
        buffer << t.rdbuf();

        string content = buffer.str();
        return content;
    }
}
