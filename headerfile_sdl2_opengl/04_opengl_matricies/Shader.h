#pragma once

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "utils.h"

using namespace std;

class Shader
{
public:
    Shader() {
        _matrixIDSet = false;
    }

    void useProgram() const { glUseProgram(_programId); }
    void setMatrix(const glm::mat4 value)
    {
        if (!_matrixIDSet) {
            _matrixID = glGetUniformLocation(_programId, "mvp");
            _matrixIDSet = true;
        }
        glUniformMatrix4fv(_matrixID, 1, GL_FALSE, &value[0][0]);
    }


    bool init()
    {
        cout << "Shader: init" << endl;
        _programId = glCreateProgram();
        return true;
    }

    bool loadShader(const string &filename, GLenum shaderType)
    {
        cout << "Shader: loading shader: " << filename << endl;

        int shaderId = _createShader(filename, shaderType);
        if (_tryCompileShader(shaderId)) {
            glAttachShader(_programId, shaderId);
            _shaderIds.push_back(shaderId);
            return true;
        }
        return false;
    }

    bool linkShaders()
    {
        glLinkProgram(_programId);

        int success = 0;
        glGetProgramiv(_programId, GL_LINK_STATUS, &success);

        cout << "link success: " << success << endl;

        if (success == 0) {
            utils::printShaderLinkingError(_programId);
            return false;
        }

        cout << "Shader: linking successfull" << endl;
        return true;
    }


    void cleanup()
    {
        cout << "Shader: cleanup" << endl;
        glUseProgram(0);

        for (auto i : _shaderIds) {
            glDetachShader(_programId, i);
        }

        glDeleteProgram(_programId);

        for (auto i : _shaderIds) {
            glDeleteShader(i);
        }
    }

private:

    int _createShader(const string &filename, GLenum shaderType)
    {
        string rawShader = utils::readShaderFile(filename.c_str());

        char* src = const_cast<char*>(rawShader.c_str());
        int32_t srcLen = rawShader.length();

        int shaderId = glCreateShader(shaderType);
        glShaderSource(shaderId, 1, &src, &srcLen);
        return shaderId;
    }

    bool _tryCompileShader(int id)
    {
        glCompileShader(id);
        int success = 0;
        glGetShaderiv(id, GL_COMPILE_STATUS, &success);

        if (success == 0) {
            utils::printShaderCompilationError(id);
        }

        return success != 0;
    }

    GLuint _programId;
    GLuint _vShaderId, fShaderId;

    GLuint _matrixID;
    bool _matrixIDSet;

    vector<int32_t> _shaderIds;
};
