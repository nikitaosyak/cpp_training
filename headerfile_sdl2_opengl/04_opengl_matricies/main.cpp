#include "Renderer.h"
#include "Model.h"
#include "sdl_wrapper.h"

#include <iostream>

Renderer render;
Model model;

void run()
{
    while(!sdl_wrapper::needToQuit()) {
        sdl_wrapper::update();

        model.rotateMatrix(0.01f);

        render.renderClear();
        render.setModelMatrix(model.getMatrix());
        render.renderModel(model);
        render.renderSwap(sdl_wrapper::getWindow());;
    }
}

int main(int argc, char** argv)
{
//    if (!r.init(512, 512)) return -1;
    if (!sdl_wrapper::init("opengl matricies", 512, 512)) return -1;

    render.init();
    render.setupShader("shader/vert.glsl", "shader/frag.glsl");
    
    if (!model.setupBufferObjects()) return -1;
    model.resetMatrix();

    render.setModelShader(model);

    run();

    render.cleanup();
    model.cleanup();
    sdl_wrapper::cleanup();

    return 0;
}
