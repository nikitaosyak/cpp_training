#pragma once

#include <GL/glew.h>
#include <string>

#define GL3_PROTOTYPES 1
#include <iostream>
#include <SDL2/SDL.h>

namespace sdl_wrapper {

    bool init();
    bool init(const char*, int, int);
    void update();
    void cleanup();
    void _checkDriverInfo(const char*, int);
    void _sdlErrorHandler(const char*, const char*, int);


    bool _initialized = false;
    SDL_Window* _window;
    SDL_GLContext _context;
    bool _quit = false;

    SDL_Window* getWindow() { return _window; }
    bool needToQuit() { return _quit; }


    bool init() { return init("unnamed sdl program", 512, 512); }
    bool init(const char* wTitle, int w, int h)
    {
        if (_initialized) return true;
        std::cout << "SDL will initialize with window size: " << w << "x" << h << std::endl;

        _initialized = true;

        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
            _sdlErrorHandler("sdl cannot be initialized", "Renderer.h", __LINE__);
            _initialized = false;
        } else {
            int cPos = SDL_WINDOWPOS_CENTERED;
            Uint32 flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;
            _window = SDL_CreateWindow(wTitle, cPos, cPos, w, h, flags);
            if (_window == NULL) {
                _sdlErrorHandler("sdl window cannot be created", "Renderer.h", __LINE__);
                _initialized = false;
            } else {

                _checkDriverInfo("sdl_wrapper.h", __LINE__);
//                setGLAttributes();

                _context = SDL_GL_CreateContext(_window);
                if (_context == NULL) {
                    _sdlErrorHandler("gl cnotext cannot be created", "Renderer.h", __LINE__);
                    _initialized = false;
                } else {
                    SDL_GL_SetSwapInterval(1); // vsync

                    glewExperimental = GL_TRUE;
                    glewInit();

//                    setGLOptions();
                    std::cout << "SDL init successfull" << std::endl;
                }
            }
        }

        return _initialized;
    }

    void update()
    {
        SDL_Event e;
        while(SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                _quit = true;
            }

            const Uint8* keyStates = SDL_GetKeyboardState(NULL);
            if (keyStates[SDL_SCANCODE_ESCAPE]) {
                _quit = true;
            }
        }
    }

    void cleanup()
    {
        cout << "sdl_wrapper: cleanup" << endl;
        SDL_GL_DeleteContext(_context);
        SDL_DestroyWindow(_window);
        SDL_Quit();

        _initialized = false;
        _quit = true;
    }

    //
    // Private methods
    //

    void _checkDriverInfo(const char* caller, int line)
        {
            cout << "----------------" << endl;

            int drAvailable = SDL_GetNumRenderDrivers();
            if (drAvailable < 1) {
                _sdlErrorHandler("No available drivers!", caller, line);
            } else {
                cout << "drivers found: " << drAvailable << endl;
                for (int i = 0; i < drAvailable; i++) {
                    SDL_RendererInfo info;
                    SDL_GetRenderDriverInfo(i, &info);
                    cout << "driver " << i << ": " << info.name << endl;
                }
            }

            cout << "----------------" << endl;
        }

    void _sdlErrorHandler(const char* m, const char* caller, int line)
    {
        string error = SDL_GetError();
        if (error != "") {
            cout << "SDL error: " << error  << " in " << caller << ":" << line << endl;
            SDL_ClearError();
        }
    }
}
