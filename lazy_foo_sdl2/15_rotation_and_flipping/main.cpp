#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "../__lib/Texture.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

Texture* tex = NULL;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl rotation and flipping", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            int flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
            renderer = SDL_CreateRenderer(window, -1, flags);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                int imageFlags = IMG_INIT_PNG | IMG_INIT_JPG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    tex = new Texture(renderer);
    success = tex->loadFromFile("assets/bear.png");

    return success;
}

void close()
{
    tex->free();
    delete tex;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    if (!loadMedia()) return 1;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);

    bool quit = false;
    SDL_Event event;

    SDL_Point center = {tex->getWidth()/2, tex->getHeight()/2};
    double angle = 0;
    double direction = 0;
    SDL_RendererFlip flipType = SDL_FLIP_NONE;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            } else if (event.type == SDL_KEYDOWN) {
                switch(event.key.keysym.sym) {
                    case SDLK_a:
                        direction = -1;
                        break;
                    case SDLK_s:
                        direction = 0;
                        break;
                    case SDLK_d:
                        direction = 1;
                        break;

                    case SDLK_q:
                        flipType = SDL_FLIP_HORIZONTAL;
                        break;
                    case SDLK_w:
                        flipType = SDL_FLIP_NONE;
                        break;
                    case SDLK_e:
                        flipType = SDL_FLIP_VERTICAL;
                        break;

                    case SDLK_f:
                        center.x = 0;
                        center.y = 0;
                        break;
                    case SDLK_g:
                        center.x = tex->getWidth()/2;
                        center.y = tex->getHeight()/2;
                        break;
                    case SDLK_h:
                        center.x = tex->getWidth();
                        center.y = tex->getHeight();
                        break;

                    case SDLK_z:
                        flipType = SDL_FLIP_NONE;
                        direction = 0;
                        angle = 0;
                        center.x = tex->getWidth()/2;
                        center.y = tex->getHeight()/2;

                        break;
                }
            }
        }

        SDL_RenderClear(renderer);

        int x = (SCREEN_WIDTH - tex->getWidth())/2;
        int y = (SCREEN_HEIGHT - tex->getHeight())/2;
        tex->render(x, y, angle, &center, flipType);

        SDL_RenderPresent(renderer);

        angle += direction;

    }

    close();
    return 0;
}