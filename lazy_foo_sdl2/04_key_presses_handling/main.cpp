#include <SDL.h>
#include <stdio.h>
#include <string>

enum KeyToSurface
{
    KEY_TO_SURFACE_UNKNOWN=0, 
    KEY_TO_SURFACE_UP, KEY_TO_SURFACE_DOWN, KEY_TO_SURFACE_LEFT, KEY_TO_SURFACE_RIGHT, 
    KEY_TO_SURFACE_COUNT
};

// function declarations
bool loadSurface(std::string, int);
bool loadSuccessCheck(SDL_Surface*);
bool init();
bool loadMedia();
void close();

// global variables
int const SCREEN_WIDTH = 640;
int const SCREEN_HEIGHT = 480;
SDL_Window* window = NULL;
SDL_Surface* windowSurface = NULL;
SDL_Surface* keyToSurfaceArray[KEY_TO_SURFACE_COUNT];
SDL_Surface* currentSurface = NULL;

//
// function defenitions
//
bool loadSurface(std::string path, int loadIdx)
{
    SDL_Surface* loadTo = SDL_LoadBMP(path.c_str());
    if (loadTo == NULL) {
        printf("unable to load surface at path: %s; error: %s\n", path.c_str(), SDL_GetError());
        return false;
    }
    keyToSurfaceArray[loadIdx] = loadTo;
    printf("loaded surface at path: %s\n", path.c_str());

    return true;
}

bool loadSuccessCheck(SDL_Surface* surf, std::string path)
{
    if (surf != NULL) return true;
    printf("failed to load bitmap at path: %s\n", path.c_str());
    return false;
}

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl could not initialize: %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl bitmap", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            windowSurface = SDL_GetWindowSurface(window);
        }
    }

    printf("init success\n");
    return success;
}

bool loadMedia()
{
    bool success = true;

    success = loadSurface("assets/bitmap.bmp", KEY_TO_SURFACE_UNKNOWN);
    success = loadSurface("assets/up.bmp", KEY_TO_SURFACE_UP);
    success = loadSurface("assets/down.bmp", KEY_TO_SURFACE_DOWN);
    success = loadSurface("assets/left.bmp", KEY_TO_SURFACE_LEFT);
    success = loadSurface("assets/right.bmp", KEY_TO_SURFACE_RIGHT);

    return success;
}

void close()
{
    for (int i = 0; i < KEY_TO_SURFACE_COUNT; i++) {
        SDL_FreeSurface(keyToSurfaceArray[i]);
        keyToSurfaceArray[i] = NULL;
    }

    SDL_DestroyWindow(window);
    window = NULL;

    SDL_Quit();
    printf("closing..\n");
}

int main(int argc, char** argv)
{
    printf("starting..\n");
    if (!init()) return 1;
    if (!loadMedia()) return 1;

    currentSurface = keyToSurfaceArray[KEY_TO_SURFACE_UNKNOWN];
    
    bool quit = false;
    SDL_Event event;

    while (!quit) {
        while (SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                quit = true;
            } else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym) 
                {
                    case SDLK_UP:
                        currentSurface = keyToSurfaceArray[KEY_TO_SURFACE_UP];
                    break;
                    case SDLK_DOWN:
                        currentSurface = keyToSurfaceArray[KEY_TO_SURFACE_DOWN];
                    break;
                    case SDLK_LEFT:
                        currentSurface = keyToSurfaceArray[KEY_TO_SURFACE_LEFT];
                    break;
                    case SDLK_RIGHT:
                        currentSurface = keyToSurfaceArray[KEY_TO_SURFACE_RIGHT];
                    break;
                    default:
                        currentSurface = keyToSurfaceArray[KEY_TO_SURFACE_UNKNOWN];
                }
            } else if (event.type == SDL_KEYUP) {
                currentSurface = keyToSurfaceArray[KEY_TO_SURFACE_UNKNOWN];
            }

            SDL_BlitSurface(currentSurface, NULL, windowSurface, NULL);
            SDL_UpdateWindowSurface(window);
        }
    }

    close();
    return 0;
}