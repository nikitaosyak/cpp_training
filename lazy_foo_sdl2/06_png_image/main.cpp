#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
SDL_Surface* loadSurface(std::string);
void close();

SDL_Window* window = NULL;
SDL_Surface* windowSurface = NULL;
SDL_Surface* image1 = NULL;
SDL_Surface* image2 = NULL;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl jpg/png", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            // initialize png load
            int imageFlags = IMG_INIT_PNG | IMG_INIT_JPG;
            if (!(IMG_Init(imageFlags) & imageFlags)) {
                printf("SDL_Image could not initialize with given flags: %s\n", SDL_GetError());
                success = false;
            } else {
                windowSurface = SDL_GetWindowSurface(window);
            }
        }
    }

    return success;
}

SDL_Surface* loadSurface(std::string path)
{
    SDL_Surface* optimizedSurface = NULL;
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());
    if (loadedSurface == NULL) {
        printf("cannot load image %s: %s\n", path.c_str(), SDL_GetError());
    } else {
        optimizedSurface = SDL_ConvertSurface(loadedSurface, windowSurface->format, 0);
        if (optimizedSurface == NULL) {
            printf("cannot optimize image %s: %s\n", path.c_str(), SDL_GetError());
        }

        SDL_FreeSurface(loadedSurface);
    }

    return optimizedSurface;
}

void close()
{
    SDL_FreeSurface(image1);
    image1 = NULL;
    SDL_FreeSurface(image2);
    image2 = NULL;

    SDL_DestroyWindow(window);
    window = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    image1 = loadSurface("assets/bear.png");
    image2 = loadSurface("assets/bear.jpg");

    bool quit = false;
    SDL_Event event;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            }
        }

        SDL_Rect image1Rect = {0, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT};
        SDL_Rect image2Rect = {SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT};
        SDL_BlitScaled(image1, NULL, windowSurface, &image1Rect);
        SDL_BlitScaled(image2, NULL, windowSurface, &image2Rect);
        SDL_UpdateWindowSurface(window);
    }

    close();
    return 0;
}