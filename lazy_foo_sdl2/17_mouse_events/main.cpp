#include <SDL.h>
#include <SDL_image.h>

#include <stdio.h>
#include <string>
#include "../__lib/Texture.h"
#include "../__lib/Button.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

Texture* tex = NULL;
Button* b1;
Button* b2;
SDL_Rect spriteClips[4];

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl mouse events", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            int flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
            renderer = SDL_CreateRenderer(window, -1, flags);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                int imageFlags = IMG_INIT_PNG | IMG_INIT_JPG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    tex = new Texture(renderer);
    success = tex->loadFromFile("assets/spritesheet.png");

    spriteClips[0] = {0, 0, 256, 256};
    spriteClips[1] = {256, 0, 256, 256};
    spriteClips[2] = {512, 0, 256, 256};
    spriteClips[3] = {768, 0, 256, 256};

    printf("before buttons\n");
    b1 = new Button(0, 0, tex, spriteClips);
    b2 = new Button(256, 0, tex, spriteClips);
    printf("after buttons\n");

    return success;
}

void close()
{
    delete b1;
    b1 = NULL;
    delete b2;
    b2 = NULL;

    delete tex;
    tex = NULL;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    if (!loadMedia()) return 1;

    printf("media load success\n");
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);

    bool quit = false;
    SDL_Event event;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            }

            b1->handleEvent(&event);
            b2->handleEvent(&event);
        }

        SDL_RenderClear(renderer);
        b1->render();
        b2->render();
        SDL_RenderPresent(renderer);
    }

    close();
    return 0;
}