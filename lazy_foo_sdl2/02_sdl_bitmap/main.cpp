#include <SDL.h>
#include <stdio.h>


//
// "interface" declaration
//
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Surface* windowSurface = NULL;
SDL_Surface* bitmapSurface = NULL;

//
//
//
bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl could not initialize: %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl bitmap", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            windowSurface = SDL_GetWindowSurface(window);
        }
    }

    return success;
}

bool loadMedia()
{
    bitmapSurface = SDL_LoadBMP("assets/bitmap.bmp");
    if (bitmapSurface == NULL) {
        printf("unable to load image: %s\n", SDL_GetError());
        return false;
    }
    return true;
}

void close()
{
    SDL_FreeSurface(bitmapSurface);
    bitmapSurface = NULL;

    SDL_DestroyWindow(window);
    window = NULL;

    SDL_Quit();
}

//
//
//
int main(int argc, char** argv)
{
    if (!init()) return 1;
    
    if (!loadMedia()) return 1;

    int blitResult = SDL_BlitSurface(bitmapSurface, NULL, windowSurface, NULL);
    if (blitResult < 0) {
        printf("unable to blit image: %s\n", SDL_GetError());
        return 1;
    }

    int updateResult = SDL_UpdateWindowSurface(window);
    if (updateResult < 0) {
        printf("unable to update surface: %s\n", SDL_GetError());
        return 1;
    }

    SDL_Delay(2000);
    close();
    return 0;
}