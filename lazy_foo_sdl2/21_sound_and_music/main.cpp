#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include <stdio.h>
#include <string>

#include "../__lib/Texture.h"

const char* SCREEN_TITLE = "sound and music";
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

const int DEADZONE = 500;

bool init();
bool setupJoystick();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

Texture* tex = NULL;

Mix_Music* music = NULL;
Mix_Chunk* high = NULL;
Mix_Chunk* medium = NULL;
Mix_Chunk* low = NULL;


bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {


        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow(SCREEN_TITLE, xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {

            // attepmt to look for drivers
            int drAvailable = SDL_GetNumRenderDrivers();
            if (drAvailable < 1) {
                printf("no drivers found: %s\n", SDL_GetError());
            } else {
                printf("drivers found: %i\n", drAvailable);
                for (int i = 0; i < drAvailable; i++) {
                    SDL_RendererInfo renderInfo;
                    SDL_GetRenderDriverInfo(i, &renderInfo);
                    printf("driver %i: %s\n", i, renderInfo.name);
                }
            }

            int rendererFlags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
            renderer = SDL_CreateRenderer(window, -1, rendererFlags);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {

                int imageFlags = IMG_INIT_PNG | IMG_INIT_JPG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                } else {

                    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
                        printf("mixer could not initialize: %s\n", Mix_GetError());
                        success = false;
                    }
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    tex = new Texture(renderer);
    success = tex->loadFromFile("assets/tex.png");

    music = Mix_LoadMUS("assets/tata.mp3");
    if (music == NULL) {
        printf("failed to load %s: %s\n", "assets/tata.mp3", Mix_GetError());
        success = false;
    }

    high = Mix_LoadWAV("assets/high.wav");
    if (high == NULL) {
        printf("failed to load %s: %s\n", "assets/high.wav", Mix_GetError());
        success = false;   
    }

    medium = Mix_LoadWAV("assets/medium.wav");
    if (medium == NULL) {
        printf("failed to load %s: %s\n", "assets/medium.wav", Mix_GetError());
        success = false;   
    }

    low = Mix_LoadWAV("assets/low.wav");
    if (low == NULL) {
        printf("failed to load %s: %s\n", "assets/low.wav", Mix_GetError());
        success = false;   
    }

    return success;
}

void close()
{
    tex->free();
    delete tex;

    Mix_FreeMusic(music);
    music = NULL;

    Mix_FreeChunk(high);
    Mix_FreeChunk(medium);
    Mix_FreeChunk(low);

    high = NULL; medium = NULL; low = NULL;


    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    printf("initialization success\n");

    if (!loadMedia()) return 1;
    printf("load media success\n");

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);

    bool quit = false;
    SDL_Event event;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            } else if (event.type == SDL_KEYDOWN) {
                switch(event.key.keysym.sym) {
                    case SDLK_1:
                        Mix_PlayChannel(-1, high, 0);
                        break;
                    case SDLK_2:
                        Mix_PlayChannel(-1, medium, 0);
                        break;
                    case SDLK_3:
                        Mix_PlayChannelTimed(-1, low, 0, 100);
                        break;
                    case SDLK_9:
                        if (Mix_PlayingMusic() == 0) {
                            Mix_PlayMusic(music, -1);
                        } else {
                            if (Mix_PausedMusic() == 1) {
                                Mix_ResumeMusic();
                            } else {
                                Mix_PauseMusic();
                            }
                        }
                        break;
                    case SDLK_0:
                        Mix_HaltMusic();
                        break;
                }
            }

            const Uint8* keyStates = SDL_GetKeyboardState(NULL);
            if (keyStates[SDL_SCANCODE_ESCAPE]) {
                quit = true;
            }

            // insert event handling here
        }

        SDL_RenderClear(renderer);
        tex->render();
        SDL_RenderPresent(renderer);
    }

    close();
    return 0;
}