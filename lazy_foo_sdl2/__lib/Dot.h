#ifndef MOVE_DOT_H
#define MOVE_DOT_H
#endif

#include "Texture.h"
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

class Dot
{
public:
    static const int DOT_WIDTH = 32;
    static const int DOT_HEIGHT = 32;
    static const int DOT_VEL = 200;

    Dot();
    void setPosition(int x, int y);
    void handleEvent(SDL_Event& e);
    void move(float dt);
    void render(Texture* tex);
    void render(int camerax, int cameray, Texture* tex);

    int getX();
    int getY();

private:
    float _px, _py;
    float _vx, _vy;
};