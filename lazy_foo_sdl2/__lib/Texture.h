#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#ifdef TEXTURE_TTF_SUPPORT
#include <SDL_ttf.h>
#endif

using namespace std;

class Texture
{
public:
    Texture(SDL_Renderer*);
    #ifdef TEXTURE_TTF_SUPPORT
    Texture(SDL_Renderer*, string, int);
    #endif
    ~Texture();

    bool loadFromFile(string);
    bool loadFromFile(string, bool, SDL_Color*);
    #ifdef TEXTURE_TTF_SUPPORT
    bool generateText(string, SDL_Color);
    #endif

    void free();

    void modColor(Uint8, Uint8, Uint8);

    void setBlendMode(SDL_BlendMode);
    void setAlpha(Uint8 alpha);

    void render();
    void render(int x, int y);
    void render(int x, int y, SDL_Rect* clip);
    void render(int x, int y, double angle, SDL_Point* center, SDL_RendererFlip flip);
    void renderFullScreen();
    void renderFullScreen(SDL_Rect* clip);

    int getWidth();
    int getHeight();

private:
    void _init();
    bool _checkTexture(const char*);

    SDL_Renderer* _renderer;
    #ifdef TEXTURE_TTF_SUPPORT
    TTF_Font* _font;
    #endif

    SDL_Texture* _tex;
    SDL_Rect _renderQuad;

    const char* _path;
    int _width;
    int _height;
};

#endif