#include "Button.h"

Button::Button(int x, int y, Texture* spriteSheet, SDL_Rect* states) 
{ 
    _tex = spriteSheet;
    _position = {x, y};
    _size = {states->w, states->h};

    _states = states;
    _currentState = BUTTON_STATE_OUT;
}

Button::~Button() { _free(); }

void Button::_free()
{
    delete _states;
    _states = NULL;
}

void Button::setPosition(int x, int y)
{
    _position.x = x;
    _position.y = y;
}

void Button::setVirtualSize(int w, int h)
{
    _size.x = w;
    _size.y = h;
}

void Button::handleEvent(SDL_Event* e)
{
    bool up = e->type == SDL_MOUSEBUTTONUP;
    bool down = e->type == SDL_MOUSEBUTTONDOWN;
    bool move = e->type == SDL_MOUSEMOTION;
    if (!up && !down && !move) return;

    int x, y;
    SDL_GetMouseState(&x, &y);

    bool inside = true;
    if (x < _position.x) inside = false;
    if (x > _position.x + _size.x) inside = false;
    if (y < _position.y) inside = false;
    if (y > _position.y + _size.y) inside = false;

    ButtonState prevState = _currentState;
    if (!inside) {
        _currentState = BUTTON_STATE_OUT;
    } else {
        switch(e->type) {
            case SDL_MOUSEMOTION:
                _currentState = BUTTON_STATE_OVER;
            break;
            case SDL_MOUSEBUTTONDOWN:
                _currentState = BUTTON_STATE_DOWN;
            break;
            case SDL_MOUSEBUTTONUP:
                _currentState = BUTTON_STATE_UP;
            break;
            default:
                printf("unknown event type %i\n", e->type);
            break;
        }
    }

    if (prevState != _currentState) {
        printf("state change: {%i} -> {%i}\n", prevState, _currentState);
    }
}

void Button::render()
{
    _tex->render(_position.x, _position.y, &_states[_currentState]);
}

