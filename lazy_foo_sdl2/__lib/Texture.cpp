#include "Texture.h"

Texture::Texture(SDL_Renderer* r) 
{
    printf("creating texture with renderer\n"); 
    _renderer = r;
    _init();
}

#ifdef TEXTURE_TTF_SUPPORT
Texture::Texture(SDL_Renderer* r, string fontPath, int fontSize)
{
    _init();

    _path = fontPath.c_str();
    printf("creating texture with ttf support (%s)\n", _path); 
    
    TTF_Font* f = TTF_OpenFont(_path, fontSize);
    if (f == NULL) {
        printf("failed to load font %s: %s\n", _path, TTF_GetError());
        _renderer = NULL;
        _font = NULL;
        _path = NULL;
    }

    _renderer = r;
    _font = f;
}
#endif

void Texture::_init()
{
    _tex = NULL;
    _renderQuad = {0, 0, -1, -1};
    _width = _height = -1;
    _path = NULL;
    printf("texture initialized\n");
}

Texture::~Texture()
{
    free();
    _renderer = NULL;
}

int Texture::getWidth() { return _width; }
int Texture::getHeight() { return _height; }

bool Texture::loadFromFile(string path)
{
    return loadFromFile(path, false, NULL);
}

bool Texture::loadFromFile(string path, bool useColorKey, SDL_Color* colorKey)
{
    printf("loading texture from file\n");
    if (_checkTexture(NULL)) free();

    _path = path.c_str();
    SDL_Texture* tex = NULL;
    SDL_Surface* surf = IMG_Load(_path);
    if (surf == NULL) {
        printf("unable to load surface %s: %s\n", _path, IMG_GetError());
    } else {
        if (useColorKey) {
            Uint32 formattedColor = SDL_MapRGB(surf->format, colorKey->r, colorKey->g, colorKey->b);
            int keyRingResult = SDL_SetColorKey(surf, SDL_TRUE, formattedColor);
            if (keyRingResult != 0) {
                printf("unable to enable color key on %s: %s\n", _path, SDL_GetError());
            }
        } 

        tex = SDL_CreateTextureFromSurface(_renderer, surf);
        if (tex == NULL) {
            printf("unable to create texture %s: %s\n", _path, SDL_GetError());
        } else {
            _width = _renderQuad.w = surf->w;
            _height = _renderQuad.h = surf->h;
        }

        SDL_FreeSurface(surf);
    }

    printf("creating texture from file %s: SUCCESS\n", _path);
    _tex = tex;
    return _tex != NULL;
}

#ifdef TEXTURE_TTF_SUPPORT
bool Texture::generateText(string text, SDL_Color textColor)
{
    // if (_checkTexture(NULL)) free();
    if (_font == NULL) {
        printf("font absent, text '%s' could not be generated\n", text.c_str());
        return false;
    }

    SDL_Surface* textSurface = TTF_RenderText_Solid(_font, text.c_str(), textColor);
    if (textSurface == NULL) {
        printf("unable to render text surface: %s\n", TTF_GetError());
    } else {
        _tex = SDL_CreateTextureFromSurface(_renderer, textSurface);
        if (_tex == NULL) {
            printf("unable to create text texture: %s\n", SDL_GetError());
        } else {
            _width = _renderQuad.w = textSurface->w;
            _height = _renderQuad.h = textSurface->h;
        }

        SDL_FreeSurface(textSurface);
    }

    return _tex != NULL;
}
#endif

void Texture::free()
{
    #ifdef TEXTURE_TTF_SUPPORT
    if (_font != NULL) {
        printf("font is not null. releasing font %s\n", _path);
        TTF_CloseFont(_font);
        _font = NULL;
    }
    #endif
    if (!_checkTexture(NULL)) return;

    printf("texture is not null. releasing texture %s\n", _path);
    SDL_DestroyTexture(_tex);
    _tex = NULL;
    _renderQuad.w = _renderQuad.h = -1;
}

void Texture::modColor(Uint8 r, Uint8 g, Uint8 b)
{
    if (!_checkTexture("mod color")) return;
    if (SDL_SetTextureColorMod(_tex, r, g, b) != 0) {
        printf("unable to mod color on texture %s: %s\n", _path, SDL_GetError());
    }
}

void Texture::setBlendMode(SDL_BlendMode mode)
{
    if (!_checkTexture("set blend mode")) return;
    if (SDL_SetTextureBlendMode(_tex, mode) != 0) {
        printf("unable to set blend mode to texture %s: %s\n", _path, SDL_GetError());
    }
}

void Texture::setAlpha(Uint8 value)
{
    if (!_checkTexture("set alpha value")) return;
    if (SDL_SetTextureAlphaMod(_tex, value) != 0) {
        printf("unable to set alpha value to texture %s: %s\n", _path, SDL_GetError());
    }
}

void Texture::render() { render(0, 0, NULL); }
void Texture::render(int x, int y) { render(x, y, NULL); }
void Texture::render(int x, int y, SDL_Rect* clip)
{
    if (!_checkTexture("render")) return;

    _renderQuad.x = x;
    _renderQuad.y = y;

    if (clip == NULL) {
        _renderQuad.w = _width;
        _renderQuad.h = _height;
    } else {
        _renderQuad.w = clip->w;
        _renderQuad.h = clip->h;
    }
    
    SDL_RenderCopy(_renderer, _tex, clip, &_renderQuad);
}
void Texture::render(int x, int y, double angle, SDL_Point* center, SDL_RendererFlip flip) {
    if (!_checkTexture("render with flip")) return;

    _renderQuad.x = x;
    _renderQuad.y = y;

    _renderQuad.w = _width;
    _renderQuad.h = _height;

    SDL_RenderCopyEx(_renderer, _tex, NULL, &_renderQuad, angle, center, flip);
}

void Texture::renderFullScreen()
{
    renderFullScreen(NULL);
}
void Texture::renderFullScreen(SDL_Rect* clip)
{
    if (!_checkTexture("render to full screen")) return;
    SDL_RenderCopy(_renderer, _tex, clip, NULL);   
}

bool Texture::_checkTexture(const char* op)
{
    if (_tex == NULL) {
        if (op != NULL) {
            printf("unable to %s, no texture loaded\n", op);
        }
        return false;
    }
    return true;
}