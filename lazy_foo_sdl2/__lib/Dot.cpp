
#include "Dot.h"

Dot::Dot()
{
    _px = _py = 0.f;
    _vx = _vy = 0.f;
}

void Dot::setPosition(int x, int y)
{
    _px = x;
    _py = y;
}

void Dot::handleEvent(SDL_Event& e)
{
    if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
        switch(e.key.keysym.sym) 
        {
            case SDLK_UP: _vy -= DOT_VEL; break;
            case SDLK_DOWN: _vy += DOT_VEL; break;
            case SDLK_LEFT: _vx -= DOT_VEL; break;
            case SDLK_RIGHT: _vx += DOT_VEL; break;
        }
    } else if (e.type == SDL_KEYUP && e.key.repeat == 0) {
        switch(e.key.keysym.sym) 
        {
            case SDLK_UP: _vy += DOT_VEL; break;
            case SDLK_DOWN: _vy -= DOT_VEL; break;
            case SDLK_LEFT: _vx += DOT_VEL; break;
            case SDLK_RIGHT: _vx -= DOT_VEL; break;
        }
    }
}

void Dot::move(float dt)
{
    _px += _vx * dt;
    _py += _vy * dt;
}

void Dot::render(Texture* tex)
{
    tex->render((int)_px, (int)_py);
}

void Dot::render(int camerax, int cameray, Texture* tex)
{
    tex->render((int)_px - camerax, (int)_py - cameray);
}

int Dot::getX() { return (int)_px; }
int Dot::getY() { return (int)_py; }