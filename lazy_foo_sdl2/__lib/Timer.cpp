
#include "Timer.h"

Timer::Timer()
{
    _startTime = 0;
    _pauseTime = 0;

    _running = false;
    _paused = false;
}

void Timer::start()
{
    // printf("timer will start\n");
    _running = true;
    _paused = false;

    _startTime = SDL_GetTicks();
    _pauseTime = 0;
}

void Timer::stop()
{
    // printf("timer will stop\n");
    _running = _paused = false;
    _startTime = _pauseTime = 0;
}

void Timer::pause()
{
    if (!_running || _paused) return;
    // printf("timer will pause\n");

    _paused = true;
    _pauseTime = SDL_GetTicks() - _startTime;
    _startTime = 0;
}

void Timer::resume()
{
    if (!_running || !_paused) return;
    // printf("timer will unpause\n");

    _paused = false;
    _startTime = SDL_GetTicks() - _pauseTime;
    _pauseTime = 0;
}

Uint32 Timer::getTime()
{
    if (!_running) return 0;

    if (_paused) return _pauseTime;

    return SDL_GetTicks() - _startTime;
}

bool Timer::isRunning() { return _running; }
bool Timer::isPaused() { return _paused && _running; }