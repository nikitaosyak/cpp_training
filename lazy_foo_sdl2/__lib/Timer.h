#ifndef TIMER_H
#define TIMER_H
#endif

#include <stdio.h>
#include <SDL.h>

class Timer
{

public:
    Timer();

    void start();
    void stop();
    void pause();
    void resume();

    Uint32 getTime();

    bool isRunning();
    bool isPaused();

private:
    Uint32 _startTime;
    Uint32 _pauseTime;

    bool _running;
    bool _paused;

};