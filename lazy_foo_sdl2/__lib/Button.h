#include <stdio.h>
#include <SDL.h>
#include "Texture.h"

const int DEFAULT_X = 0;
const int DEFAULT_Y = 0;

enum ButtonState
{
    BUTTON_STATE_OUT, BUTTON_STATE_OVER, BUTTON_STATE_DOWN, BUTTON_STATE_UP,
    BUTTON_STATE_COUNT
};

class Button
{
public:
    Button(int, int, Texture*, SDL_Rect*);

    ~Button();

    void setVirtualSize(int, int);
    void setPosition(int, int);
    void handleEvent(SDL_Event*);

    void render();

private:
    void _free();
    SDL_Point _position;
    SDL_Point _size;

    Texture* _tex;
    SDL_Rect* _states;
    ButtonState _currentState;
};