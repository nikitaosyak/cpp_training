#include <SDL.h>
#include <stdio.h>


//
// "interface" declaration
//
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Surface* windowSurface = NULL;
SDL_Surface* bitmapSurface = NULL;

//
//
//
bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl could not initialize: %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl bitmap", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            windowSurface = SDL_GetWindowSurface(window);
        }
    }

    printf("init success\n");
    return success;
}

bool loadMedia()
{
    bitmapSurface = SDL_LoadBMP("assets/bitmap.bmp");
    if (bitmapSurface == NULL) {
        printf("unable to load image: %s\n", SDL_GetError());
        return false;
    }
    printf("load media success\n");
    return true;
}

void close()
{
    SDL_FreeSurface(bitmapSurface);
    bitmapSurface = NULL;

    SDL_DestroyWindow(window);
    window = NULL;

    SDL_Quit();
    printf("closing..\n");
}

//
//
//
int main(int argc, char** argv)
{
    printf("starting..\n");
    if (!init()) return 1;
    
    if (!loadMedia()) return 1;

    bool quit = false;
    SDL_Event event;

    while(!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT event! \n");
                quit = true;
            }
        }
        SDL_BlitSurface(bitmapSurface, NULL, windowSurface, NULL);
        SDL_UpdateWindowSurface(window);
    }

    close();
    return 0;
}