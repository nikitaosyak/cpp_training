#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
bool loadMedia();
SDL_Texture* loadTexture(std::string);
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Texture* texture;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl viewport", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                int imageFlags = IMG_INIT_PNG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

SDL_Texture* loadTexture(std::string path)
{
    SDL_Texture* tex = NULL;
    SDL_Surface* image = IMG_Load(path.c_str());
    if (image == NULL) {
        printf("unable to load image %s: %s\n", path.c_str(), SDL_GetError());
    } else {
        tex = SDL_CreateTextureFromSurface(renderer, image);
        if (tex == NULL) {
            printf("unable to create texture from %s: %s\n", path.c_str(), SDL_GetError());
        }

        SDL_FreeSurface(image);
    }

    return tex;
}

bool loadMedia()
{
    texture = loadTexture("assets/bear.png");
    return texture != NULL;
}

void close()
{
    SDL_DestroyTexture(texture);
    texture = NULL;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);
    
    if (!loadMedia()) return 1;

    bool quit = false;
    SDL_Event event;

    SDL_Rect topLeftViewport = {0, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT/2};
    SDL_Rect topRightViewport = {SCREEN_WIDTH/2, 0, SCREEN_WIDTH/2, SCREEN_HEIGHT/2};
    SDL_Rect bottomViewport = {0, SCREEN_HEIGHT/2, SCREEN_WIDTH, SCREEN_HEIGHT/2};

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            }
        }

        SDL_RenderClear(renderer);
        SDL_RenderSetViewport(renderer, &topLeftViewport);
        SDL_RenderCopy(renderer, texture, NULL, NULL);

        SDL_RenderSetViewport(renderer, &topRightViewport);
        SDL_RenderCopy(renderer, texture, NULL, NULL);

        SDL_RenderSetViewport(renderer, &bottomViewport);
        SDL_RenderCopy(renderer, texture, NULL, NULL);

        SDL_RenderPresent(renderer);
    }

    close();
    return 0;
}