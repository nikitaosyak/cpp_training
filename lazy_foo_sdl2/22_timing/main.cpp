#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include <stdio.h>
#include <string>
#include <sstream>

#include "../__lib/Texture.h"

const char* SCREEN_TITLE = "timer";
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

Texture* promptTex = NULL;
Texture* timeTex = NULL;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {


        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow(SCREEN_TITLE, xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {

            // attepmt to look for drivers
            int drAvailable = SDL_GetNumRenderDrivers();
            if (drAvailable < 1) {
                printf("no drivers found: %s\n", SDL_GetError());
            } else {
                printf("drivers found: %i\n", drAvailable);
                for (int i = 0; i < drAvailable; i++) {
                    SDL_RendererInfo renderInfo;
                    SDL_GetRenderDriverInfo(i, &renderInfo);
                    printf("driver %i: %s\n", i, renderInfo.name);
                }
            }

            int rendererFlags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
            renderer = SDL_CreateRenderer(window, -1, rendererFlags);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {

                int imageFlags = IMG_INIT_PNG | IMG_INIT_JPG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                }

                if (TTF_Init() == -1) {
                    printf("could not initialize SDL_TTF: %s\n", TTF_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    promptTex = new Texture(renderer, "assets/true_lies.ttf", 28);
    success = promptTex->generateText("Enter to reset timer", {0xFF, 0xFF, 0xFF});

    timeTex = new Texture(renderer, "assets/true_lies.ttf", 26);
    success = timeTex->generateText("ms from start: 0", {0xFF, 0xFF, 0xFF});

    return success;
}

void close()
{
    promptTex->free();
    delete promptTex;

    timeTex->free();
    delete timeTex;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    printf("initialization success\n");

    if (!loadMedia()) return 1;
    printf("load media success\n");

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);

    bool quit = false;
    SDL_Event event;

    Uint32 startTime = 0;
    std::stringstream timeText;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            } else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN) {
                startTime = SDL_GetTicks();
            }
        }

        timeText.str("");
        timeText << "ms since start: " << SDL_GetTicks() - startTime;

        if (!timeTex->generateText(timeText.str().c_str(), {0xff, 0xff, 0xff})) {
            printf("unable to render text to texture\n");
            quit = true;
        }

        SDL_RenderClear(renderer);
        int x = (SCREEN_WIDTH - promptTex->getWidth())/2;
        promptTex->render(x, 20);

        x = (SCREEN_WIDTH - timeTex->getWidth())/2;
        int y = (SCREEN_HEIGHT - timeTex->getHeight())/2;
        timeTex->render(x, y);

        SDL_RenderPresent(renderer);
    }

    close();
    return 0;
}