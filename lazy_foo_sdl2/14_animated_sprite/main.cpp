#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "../__lib/Texture.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

const int FRAME_DELAY = 5;
const int ANIMATION_FRAMES = 4;
SDL_Rect spriteClips[ANIMATION_FRAMES];
Texture* spriteSheet = NULL;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl sprite sheet animation", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            int flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
            renderer = SDL_CreateRenderer(window, -1, flags);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                int imageFlags = IMG_INIT_PNG | IMG_INIT_JPG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    spriteSheet = new Texture(renderer);
    success = spriteSheet->loadFromFile("assets/tilesheet.png");

    spriteClips[0] = {0, 0, 16, 16};
    spriteClips[1] = {16, 0, 16, 16};
    spriteClips[2] = {32, 0, 16, 16};
    spriteClips[3] = {48, 0, 16, 16};

    return success;
}

void close()
{
    spriteSheet->free();
    delete spriteSheet;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    if (!loadMedia()) return 1;

    bool quit = false;
    SDL_Event event;

    int frame = 0;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            } 
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);
        SDL_RenderClear(renderer);

        SDL_Rect* currentClip = &spriteClips[frame/FRAME_DELAY];
        spriteSheet->renderFullScreen(currentClip);

        SDL_RenderPresent(renderer);

        frame += 1;
        if (frame/FRAME_DELAY >= ANIMATION_FRAMES) {
            frame = 0;
        }
    }

    close();
    return 0;
}