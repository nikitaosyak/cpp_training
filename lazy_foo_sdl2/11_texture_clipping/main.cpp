#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "../__lib/Texture.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

SDL_Rect spriteClips[4];
Texture* spriteSheet = NULL;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl texture clip", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                int imageFlags = IMG_INIT_JPG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    spriteSheet = new Texture(renderer);
    success = spriteSheet->loadFromFile("assets/spritesheet.png");

    spriteClips[0] = {0, 0, 100, 100};
    spriteClips[1] = {100, 0, 100, 100};
    spriteClips[2] = {200, 0, 100, 100};
    spriteClips[3] = {300, 0, 100, 100};
    
    return success;
}

void close()
{
    spriteSheet->free();
    delete spriteSheet;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    if (!loadMedia()) return 1;

    bool quit = false;
    SDL_Event event;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            }
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);
        SDL_RenderClear(renderer);

        const int w = SCREEN_WIDTH;
        const int h = SCREEN_HEIGHT;
        spriteSheet->render(0, 0, &spriteClips[0]);
        spriteSheet->render(w - spriteClips[1].w, 0, &spriteClips[1]);
        spriteSheet->render(0, h - spriteClips[2].h, &spriteClips[2]);
        spriteSheet->render(w - spriteClips[3].w, h - spriteClips[3].h, &spriteClips[3]);

        SDL_RenderPresent(renderer);
    }

    close();
    return 0;
}