#include <SDL.h>
#include <SDL_image.h>

#include <stdio.h>
#include <string>

#include "../__lib/Texture.h"

const char* SCREEN_TITLE = "sdl joystick";
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

const int DEADZONE = 8000;

bool init();
bool setupJoystick();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

Texture* tex = NULL;
SDL_Joystick* controller = NULL;
SDL_Haptic* controllerHaptic = NULL;
int selectedController = -1;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_HAPTIC) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {


        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow(SCREEN_TITLE, xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {

            // attepmt to look for drivers
            int drAvailable = SDL_GetNumRenderDrivers();
            if (drAvailable < 1) {
                printf("no drivers found: %s\n", SDL_GetError());
            } else {
                printf("drivers found: %i\n", drAvailable);
                for (int i = 0; i < drAvailable; i++) {
                    SDL_RendererInfo renderInfo;
                    SDL_GetRenderDriverInfo(i, &renderInfo);
                    printf("driver %i: %s\n", i, renderInfo.name);
                }
            }

            int rendererFlags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
            renderer = SDL_CreateRenderer(window, -1, rendererFlags);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                int imageFlags = IMG_INIT_PNG | IMG_INIT_JPG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool setupJoystick()
{
    bool success = true;

    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
        printf("Warning: linear texture filtering not enabled\n");
    }

    int numJoys = SDL_NumJoysticks();
    if (numJoys < 1) {
        printf("Warning: no joysticks connected\n");
        success = false;
    } else {
        for (int i = 0; i < numJoys; i++) {
            printf("joy %i: '%s'\n", i, SDL_JoystickNameForIndex(i));
        }
        selectedController = numJoys-1;
        controller = SDL_JoystickOpen(selectedController);
        if (controller == NULL) {
            printf("Warning: unable to open controller: %s\n", SDL_GetError());
            success = false;
        } else {
            printf("joy axes: %d; buttons: %d; balls: %d\n", SDL_JoystickNumAxes(controller), SDL_JoystickNumButtons(controller), SDL_JoystickNumBalls(controller));

            controllerHaptic = SDL_HapticOpen(selectedController);
            if (controllerHaptic == NULL) {
                printf("Warning: controller does not support haptics: %s\n", SDL_GetError());
            } else {
                if (SDL_HapticRumbleInit(controllerHaptic) < 0) {
                    printf("Warning: Unable to initialize rumble: %s\n", SDL_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    tex = new Texture(renderer);
    success = tex->loadFromFile("assets/tux.png");

    return success;
}

void close()
{
    tex->free();
    delete tex;

    SDL_HapticClose(controllerHaptic);
    SDL_JoystickClose(controller);
    controller = NULL;
    controllerHaptic = NULL;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    printf("initialization success\n");

    if (!loadMedia()) return 1;
    printf("load media success\n");

    if (!setupJoystick()) return 1;
    printf("joystick init succes\n");

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);

    bool quit = false;
    SDL_Event event;

    int tx = 0;
    int ty = 0;
    int xdir = 0;
    int ydir = 0;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            } else if (event.type == SDL_JOYAXISMOTION) {
                if (event.jaxis.which == selectedController) {
                    if (event.jaxis.axis == 0) {
                        if (event.jaxis.value < -DEADZONE) {
                            xdir = -1;
                        } else if (event.jaxis.value > DEADZONE) {
                            xdir = 1;
                        } else {
                            xdir = 0;
                        }
                    }
                    if (event.jaxis.axis == 1) {
                        if (event.jaxis.value < -DEADZONE) {
                            ydir = -1;
                        } else if (event.jaxis.value > DEADZONE) {
                            ydir = 1;
                        } else {
                            ydir = 0;
                        }
                    }
                }
            } else if (event.type == SDL_JOYBUTTONDOWN) {
                // printf("joystick button down\n");
                if (SDL_HapticRumblePlay(controllerHaptic, 0.5, 2000) != 0) {
                    printf("Warning: unable to play haptic: %s\n", SDL_GetError());
                } else {
                    // printf("all ok?: %s\n", SDL_GetError());
                }
            }

            const Uint8* keyStates = SDL_GetKeyboardState(NULL);
            if (keyStates[SDL_SCANCODE_ESCAPE]) {
                quit = true;
            }

            // insert event handling here
        }

        if (xdir != 0) {
            tx += xdir;
        }
        if (ydir != 0) {
            ty += ydir;
        }

        SDL_RenderClear(renderer);
        // insert actions here
        tex->render(tx, ty);
        SDL_RenderPresent(renderer);
    }

    close();
    return 0;
}