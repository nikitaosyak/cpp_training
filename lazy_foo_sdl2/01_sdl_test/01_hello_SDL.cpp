#include <SDL.h>
#include <stdio.h>

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

int main(int argc, char** argv)
{
    SDL_Window* window = NULL;
    SDL_Surface* screenSurface = NULL;
    
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl could not initialize: %s", SDL_GetError());
        return 1;
    }
    
    int xPos = SDL_WINDOWPOS_CENTERED;
    int yPos = SDL_WINDOWPOS_CENTERED;
    int w = SCREEN_WIDTH;
    int h = SCREEN_HEIGHT;
    Uint32 flags = SDL_WINDOW_SHOWN;
    window = SDL_CreateWindow("sdl test", xPos, yPos, w, h, flags);
    if (window == NULL) {
        printf("window could not be created: %d", SDL_GetError());
        return 2;
    }
    
    screenSurface = SDL_GetWindowSurface(window);
    Uint32 color = SDL_MapRGB(screenSurface->format, 255, 255, 0);
    SDL_FillRect(screenSurface, NULL, color);
    SDL_UpdateWindowSurface(window);
    
    SDL_Delay(2000);
    
    SDL_DestroyWindow(window);
    
    SDL_Quit();
    
    return 0;
}