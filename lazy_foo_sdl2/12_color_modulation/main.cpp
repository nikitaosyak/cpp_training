#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "../__lib/Texture.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

bool init();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

Texture* tex = NULL;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {
        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow("sdl color modulation", xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                int imageFlags = IMG_INIT_JPG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    tex = new Texture(renderer);
    success = tex->loadFromFile("assets/mod_texture.bmp");

    return success;
}

void close()
{
    tex->free();
    delete tex;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    if (!loadMedia()) return 1;

    bool quit = false;
    SDL_Event event;

    Uint8 r = 255;
    Uint8 g = 255;
    Uint8 b = 255;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            } else if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym) 
                {
                    case SDLK_q:
                        r+= 32;
                        break;
                    case SDLK_w:
                        g+=32;
                        break;
                    case SDLK_e:
                        b+=32;
                        break;
                    case SDLK_a:
                        r-=32;
                        break;
                    case SDLK_s:
                        g-=32;
                        break;
                    case SDLK_d:
                        b-=32;
                        break;
                }
            }
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);
        SDL_RenderClear(renderer);

        tex->modColor(r, g, b);
        tex->renderFullScreen();

        SDL_RenderPresent(renderer);
    }

    close();
    return 0;
}