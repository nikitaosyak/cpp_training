#include <SDL.h>
#include <SDL_image.h>

#include <stdio.h>
#include <string>

#include "../__lib/Texture.h"

const char* SCREEN_TITLE = "sdl template";
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

const int DEADZONE = 500;

bool init();
bool setupJoystick();
bool loadMedia();
void close();

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

Texture* tex = NULL;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0) {
        printf("sdl cannot be initialized %s\n", SDL_GetError());
        success = false;
    } else {


        int xPos = SDL_WINDOWPOS_CENTERED;
        int yPos = SDL_WINDOWPOS_CENTERED;
        Uint32 flags = SDL_WINDOW_SHOWN;
        window = SDL_CreateWindow(SCREEN_TITLE, xPos, yPos, SCREEN_WIDTH, SCREEN_HEIGHT, flags);
        if (window == NULL) {
            printf("window could not be created: %s\n", SDL_GetError());
            success = false;
        } else {

            // attepmt to look for drivers
            int drAvailable = SDL_GetNumRenderDrivers();
            if (drAvailable < 1) {
                printf("no drivers found: %s\n", SDL_GetError());
            } else {
                printf("drivers found: %i\n", drAvailable);
                for (int i = 0; i < drAvailable; i++) {
                    SDL_RendererInfo renderInfo;
                    SDL_GetRenderDriverInfo(i, &renderInfo);
                    printf("driver %i: %s\n", i, renderInfo.name);
                }
            }

            int rendererFlags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
            renderer = SDL_CreateRenderer(window, -1, rendererFlags);
            if (renderer == NULL) {
                printf("renderer could not be created: %s\n", SDL_GetError());
                success = false;
            } else {
                int imageFlags = IMG_INIT_PNG | IMG_INIT_JPG;
                if (!(IMG_Init(imageFlags) & imageFlags)) {
                    printf("could not initialize SDL_Image: %s\n", SDL_GetError());
                    success = false;
                }
            }
        }
    }

    return success;
}

bool loadMedia()
{
    bool success = true;

    tex = new Texture(renderer);
    // success = tex->loadFromFile("assets/tux.png");

    return success;
}

void close()
{
    tex->free();
    delete tex;

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;

    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char** argv)
{
    if (!init()) return 1;
    printf("initialization success\n");

    if (!loadMedia()) return 1;
    printf("load media success\n");

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);

    bool quit = false;
    SDL_Event event;

    while (!quit) {
        while(SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                printf("got SDL_QUIT\n");
                quit = true;
            }

            const Uint8* keyStates = SDL_GetKeyboardState(NULL);
            if (keyStates[SDL_SCANCODE_ESCAPE]) {
                quit = true;
            }

            // insert event handling here
        }

        SDL_RenderClear(renderer);
        tex->render();
        SDL_RenderPresent(renderer);
    }

    close();
    return 0;
}